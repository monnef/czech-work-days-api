{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Main where

import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Web.Scotty as SC
import qualified Network.Wai.Handler.Warp as Warp
import Web.Scotty (scottyOpts, ScottyM, ActionM, get, html, text, json, param)
import Data.Monoid ((<>))
import Safe (readMay)
import Control.Lens.Operators ((<&>))
import Data.Function ((&))
import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics
import Data.Time (Day)

import CzechWorkDays (countOfWorkdaysInMonth, getHolidaysInMonth, getNonWeekendHolidaysInMonth)

data WorkDaysInMonth = WorkDaysInMonth { forMonth :: String
                                       , workDaysCount :: Int
                                       , holidays :: [(Day, String)]
                                       , nonWeekendHolidays :: [(Day, String)]
                                       } deriving (Show, Generic)

instance ToJSON WorkDaysInMonth
instance FromJSON WorkDaysInMonth

tshow :: Show a => a -> TL.Text
tshow = TL.pack . show

aboutAction :: ActionM ()
aboutAction = html "<h1>Czech Work Days API</h1>"

readIntMay :: String -> Maybe Int
readIntMay = readMay

genWorkDaysInMonth :: Int -> Int -> WorkDaysInMonth
genWorkDaysInMonth yearInt month = res where
  year = toInteger yearInt
  apMY :: (Integer -> Int -> b) -> b
  apMY f = f year month
  res = WorkDaysInMonth { forMonth           = show year <> "-" <> show month
                        , workDaysCount      = apMY countOfWorkdaysInMonth
                        , holidays           = apMY getHolidaysInMonth
                        , nonWeekendHolidays = apMY getNonWeekendHolidaysInMonth
                        }

workDaysMonthAction :: ActionM ()
workDaysMonthAction = do
  yearMay <- param "year" <&> readIntMay
  monthMay <- param "month" <&> readIntMay
  case (yearMay, monthMay) of (Just year, Just month) -> json $ genWorkDaysInMonth year month
                              (_, _)                  -> text "invalid parameter(s)"

routes :: ScottyM ()
routes = do
  get "/" aboutAction
  get "/work-days/:year/:month" workDaysMonthAction

hostPort :: Int
hostPort = 4884

main :: IO ()
main = do
  putStrLn $ "Starting server on http://localhost:" <> show hostPort
  let settings = Warp.defaultSettings & Warp.setPort hostPort
  let options = SC.Options 0 settings
  scottyOpts options routes

-- http://localhost:4884/work-days/2014/12
-- http://localhost:4884/work-days/2017/12
